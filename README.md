![picture](https://shippmedia.s3.sa-east-1.amazonaws.com/popups/desafioshipp.png)

# Desafio Shipp Backend

### Como usar

Ambiente de desenvolvimento php 7.2 com as extensoes xml, mbstring,sqlite3

1 ) Composer update

2 ) Alterar o nome do arquivo .env.example para .env e na informações DB altera para DB_CONNECTION=sqlite

2.1 ) As proximas informações DB colocar # na frente #DB_HOST=127.0.0.1 por exemplo

3 ) Roda o comando no terminal php artisan migrate

4 ) Copie o arquivo csv para src/storage/app ficando o caminho src/storage/app/stores.csv

5 ) Roda o comando q importa a base php artisan command:importDatabase

6 ) Iniciar o serviço php -S localhost:8000 -t public

7 ) http://localhost:8000/v1/stores?latitude=42.754424&longitude=-73.700539 exemplo de url

Foi implementado middleware para verificar os parametros da rota e para registrar log antes da resposta, o log pode ser encontrado em src/storage/logs

