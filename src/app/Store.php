<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Store extends Model
{
    //

    protected $fillable = [
        'entity_name','dba_name','establishment_type',
        'operation_type','license_number','county',
        'state','city','street_name','street_number',
        'zip_code','square_footage','longitude','latitude'
    ];

    protected $connection = 'sqlite';
    private $mass = [];

    public function setMass($singleStore){

            if(array_key_exists('longitude', $singleStore)){
                $store['entity_name'] = $singleStore[4];
                $store['dba_name'] = $singleStore[5];
                $store['establishment_type'] = $singleStore[3];
                $store['operation_type'] = $singleStore[2];
                $store['license_number'] = $singleStore[1];
                $store['county'] = $singleStore[0];
                $store['state'] = $singleStore[11];
                $store['city'] = $singleStore[10];
                $store['street_name'] = $singleStore[7];
                $store['street_number'] = $singleStore[6];
                $store['zip_code'] = $singleStore[12];
                $store['square_footage'] = $singleStore[13];
                $store['longitude'] = $singleStore['longitude'];
                $store['latitude'] = $singleStore['latitude'];

                array_push($this->mass, $store);
            }

    }

    public function register(){
        if(count($this->mass) > 1){
            Store::insert($this->mass);
            $this->mass = [];
        }
            
    }

    public function test(){

        return app('db')->select('select * from stores limit 100');
    }

    public function getStores($filter){
        try {
        
        #recuperando instancia do pdo
        $pdo =  app('db')->getPdo() ;
        #injetando a função direto na base que verifica a distancia direto na sql 
        $pdo->sqliteCreateFunction('dt',function ($lat1, $lon1, $lat2, $lon2) {

        $lat1 = deg2rad((float) $lat1);
        $lat2 = deg2rad((float)$lat2);
        $lon1 = deg2rad((float)$lon1);
        $lon2 = deg2rad((float)$lon2);
        
        $dist = (6371 * acos( cos( $lat1 ) * cos( $lat2 ) * cos( $lon2 - $lon1 ) + sin( $lat1 ) * sin($lat2) ) );
        return $dist;
       },4);

       #realiza o select
       return app('db')->select("
       select distinct dba_name, street_name, 
              street_number,state, 
              city, zip_code, 
              dt(latitude, longitude,:latitude,:longitude) distance 
       from stores  
       where 
         latitude is not null 
         and longitude is not null 
         and  dt(latitude, longitude,:latitude, :longitude)  <= 6.500 
         order by distance asc ", [
             'latitude'  => $filter->latitude,
             'longitude' => $filter->longitude
         ]);

        } catch (\Exception $e) {
            return ['error' => $e->getMessage() ];
        }
       
    }

    public function delete(){
        return app('db')->delete('delete from stores');
    }   

}
