<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class ImportCSV{

    public function __construct($store) {
        $this->storeFile($store);
    }
    
    protected function storeFile($store){
   
        try {
            
            $file = fopen(storage_path('app/stores.csv'), 'r');
            print "IMPORTANTE! ESSA ETAPA DEMORA UM POUCO! \n";
            $i = 0;
            $totalInsert = 0;
            while  ( ($line = fgetcsv($file,1000)) !== false)
            {
                if($line[0] !== "County"){

                    $futureStore = $line;
                    $location =  explode(':', end( $line) ) ;

                    if( count( $location ) > 1){
                        $futureStore['longitude'] = $this->extractLong($location);
                        $futureStore['latitude']  = $this->extractLat($location);
                    }

                    $response = $store->setMass($futureStore);
                    //var_dump($futureStore);
                    print "\n";
                    print "INSERINDO REGISTROS ! JA FORAM INSERIDOS {$totalInsert} \n";
                    if($i == 100){
                        $store->register();
                        $totalInsert += $i;
                        $i = 0;
                    }
                    

                    $i++;
              }
            }
            print "FINALIZADO A IMPORTAÇÃO COM UM TOTAL DE  {$totalInsert} REGISTROS INSERIDOS! \n";
           
        
            //code...
        } catch (\Exception $e) {
            die("Nao foi possivel realizar importar loja \n" . $e->getMessage() . "\n");
        }
        fclose($file);
    }
    
    private function extractLong($location){
        
        $lon = $location[1];
        $lon = substr($lon, 0, 13)  ; 
        $lon = explode("'", $lon)[1];
        
        return ($lon == 'huma')? '' : $lon;
    }
    
    private function extractLat($location){
     
        $lat = end($location);
        $lat = substr($lat, 0, 11);	
        $lat = explode("'", $lat)[1];
        
        return ( $lat == '}')? '' : $lat;
    }
    
    }
    