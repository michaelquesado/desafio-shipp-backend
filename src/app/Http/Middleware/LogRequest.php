<?php

namespace App\Http\Middleware;

use Closure;
use Log;

class LogRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        
        $response = $next($request);
     
        Log::info('Request api: ' .
                  ' latitude, logitude ' . implode(',', $request->all() ) . 
                  ' status ' . $response->status() . 
                  ' total_stores ' . count($response->original)
                 );

        return $response;
    }
}
