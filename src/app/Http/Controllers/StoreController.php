<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function storeByLocation(\App\Store $store, Request $request){
        return response()->json($store->getStores((object) $request->all())) ;
    }

    
}
