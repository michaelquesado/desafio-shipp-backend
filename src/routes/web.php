<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;



$router->group(['prefix' => 'v1'], function () use ($router) {
    
    $router->group(['prefix' => 'stores'], function () use ($router) {
        
        $router->group(['middleware' => ['emptyParams', 'logRequest']], function () use ($router) {       

            $router->get('/','StoreController@storeByLocation');

        });
        
        #apenas para testes
        $router->delete('/', function (\App\Store $store)  {
            return response()->json( $store->delete()  ) ;
        });


    });
});




