<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity_name');
            $table->string('dba_name');
            $table->string('establishment_type');
            $table->string('operation_type');
            $table->string('license_number');
            $table->string('county');
            $table->string('state');
            $table->string('city');
            $table->string('street_name');
            $table->string('street_number');
            $table->string('zip_code');
            $table->string('square_footage');
            $table->string('longitude');
            $table->string('latitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
